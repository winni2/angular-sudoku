import { Component } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { Level } from "app/service/demo.games";

export interface Theme {
  name: string;
  // tslint:disable-next-line: no-any
  primaryColor: any;
  // tslint:disable-next-line: no-any
  accentColor: any;
  // tslint:disable-next-line: no-any
  backgroundColor: any;
}

export abstract class SidenavApp {
  abstract readonly currentTheme: string;
  abstract readonly themes: Theme[];
  abstract readonly auth: AngularFireAuth;

  abstract newGame(level: Level): void;
  abstract ownGame(): void;

  abstract solveNext(): void;
  abstract solveAll(): void;
  abstract isSolved(): boolean;

  abstract setTheme(theme: string): void;

  abstract login(): void;
  abstract logout(): void;
  abstract about(): void;
}

@Component({
  selector: "sudoku-sidenav",
  styleUrls: ["./sidenav.component.scss"],
  templateUrl: "./sidenav.component.html",
})
export class SidenavComponent {

  constructor(public app: SidenavApp) {
  }
}
